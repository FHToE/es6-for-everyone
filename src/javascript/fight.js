export function fight(firstFighter, secondFighter) {
  let firstCurrentHP = firstFighter.health;
  let secondCurrentHP = secondFighter.health;
  while (firstCurrentHP > 0 && secondCurrentHP > 0) {
    firstCurrentHP = firstCurrentHP - getDamage(secondFighter, firstFighter); 
    if (firstCurrentHP < 0) break;
    secondCurrentHP = secondCurrentHP - getDamage(firstFighter, secondFighter); 
  }
  if (firstCurrentHP > secondCurrentHP) {
    return firstFighter;
  } else {
    return secondFighter;
  }
  // return winner
}

export function getDamage(attacker, enemy) {
  let damage = getHitPower(attacker) - getBlockPower(enemy);
  if (damage < 0) {
    return 0;
  } else {
    return damage;
  }
}

export function getHitPower(fighter) {
  let criticalHitChance = Math.random() + 1;
  let hitPower = fighter.attack * criticalHitChance;
  return hitPower;
}

export function getBlockPower(fighter) {
  let dodgeChance = Math.random() + 1;
  let blockPower = fighter.defense * dodgeChance;
  return blockPower;
}